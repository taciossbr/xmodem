import serial
import time
import os 

NAK = b'\x15'
SOH = b'\x01'
EOT = b'\x04'
ACK = b'\x06'



def menu():
    print '1 - Enviar'
    print '2 - Receber'

    return raw_input('Digite a opcao desejada:')

def select_port():
    return raw_input('Digite a porta desejada:')

def select_file():
    return raw_input('Digite o caminho do arquivo:')

def checksum(pacote):
    chk = 0
    i = 0
    while i < len(pacote):
        chk = chk + ord(pacote[i])
        i = i + 1
        
    return chk % 256



def read(file):
    arq = open(file,'r')
    content = arq.read()
    i = 0
    pacotes = []
    while i < len(content)/128:
        if i == 0:
            pacotes.append(content[i*128:(i+1)*128])
        else:
            pacotes.append(content[i*128:(i+1)*128])
        i=i+1
    pacotes.append(content[i*128:(i+1)*128]+b'\x00'*(128-len(content[i*128:(i+1)*128])))
    return pacotes


def write (pacote):
    arq = open('recebido','a')
    arq.write(pacote)
    arq.close()


def send(pacote, index, ser):
    ser.write(SOH)
    ser.write(bytes(chr(index)))
    ser.write(bytes(chr(255-index)))
    ser.write(str(pacote))
    ser.write(bytes(chr(checksum(pacote))))
    


def enviar():
    # print 'enviar'
    ser = serial.Serial(select_port())
    ser.flushInput()
    file = select_file()
    if ser.read() == NAK:
        print 'NAK'
        pacotes = read(file)
        i = 1
        for pacote in pacotes:
            send(pacote, i, ser)
            if ser.read() == ACK:
                print '\r',''*50,'\rPacote', i, '... Enviado com sucesso'
                print (100/len(pacotes))*i, '% transferidos\r'
            i = i + 1
        ser.write(EOT)
        if ser.read() == ACK:
            print '100% transferidos\nOk'



def receber(): 
    # print 'receber'   
    ser = serial.Serial(select_port())
    ser.flushOutput()
    ser.write(NAK)
    # print 'NAK'
    # time.sleep(3)
    r = ser.read()
    while r == SOH:
        index = ser.read()
        inverse = ser.read()
        data = ser.read(128)
        chk = ser.read()
        print 'Pacote',ord(index),'... \tOk'
        if ord(index) + ord(inverse) == 255 :
            if ord(chk) == checksum(data):
                write(data)
                ser.write(ACK)
            else:
                print 'Checksum invalido\n',ord(chk),'\n-->NAK'
                ser.write(NAK)
        else:
            print 'index invalido\n-->NAK'
            ser.write(NAK)
        # time.sleep(3)
        r = ser.read()
        if r == EOT:
            op1 = raw_input('Deseja renomear o arquivo? [Y/n] ')
            ser.write(ACK)

            if op1 == 'Y':
                os.rename('recebido', raw_input('Digite o novo nome: '))
            break

op = menu()

if op == '1':
    enviar()
else :
    receber()
